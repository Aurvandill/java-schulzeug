 /**
 * Diese Klasse erzeugt eine skyline mit zufälligen Farben und breiten sowie höhen
 * @author (Paul H.) 
 * @version (19.11.2015)
 */
public class skyline {

Turtle paul = new Turtle(); //erschaffung der turtle paul
  public skyline()  {  
    paul.clearScreen();
    paul.zumStart (10, 100); //startpunkt...
    paul.vor (10);           //gehe 10 vor für anfang    
    for (int schleifen=0; schleifen<=5; schleifen++) //schleifendefinierung
     {
        int zufall1= (int) (1+Math.random() * 5); //zufallszahl zwischen 1 und 5
        int zufall2= (int) (10+Math.random() * 41);//zufallszahl zwischen 10 und 50
        int zufall3= (int) (10+Math.random() * 91);//zufallszahl zwischen 10 und 100
         //Zufalszahlen werden nach muster 
         //(niedrigstezahl +Math.random()* (höchstezahl-niedrigste zahl+1 )
         //gebildet
        rechteck (zufall1,zufall2,zufall3);              
      }
    paul.setzeFarbe (0); //farbe wieder schwarz
    paul.vor(10); //gehe 10 vor für ende
    
  }
public void rechteck (int farbe, int breite, int hoehe) //klasse die ein viereck geht
  {
    paul.setzeFarbe(farbe); //farbe durch zufallswert setzen
    paul.vor(breite);
    paul.drehe(-90);
    paul.vor(hoehe);
    paul.drehe(-90);
    paul.vor(breite);
    paul.drehe(-90);
    paul.vor(hoehe);
    paul.drehe(-90);
    paul.vor(breite);
    paul.setzeFarbe (0); //farbe wieder schwarz
    paul.vor(10);        //abstand zwischen gebäuden 
  }
} // Ende der Klasse skyline